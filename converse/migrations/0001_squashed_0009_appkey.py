# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    replaces = [(b'converse', '0001_initial'), (b'converse', '0002_order_paid'), (b'converse', '0003_20151214_1506'), (b'converse', '0004_auto_20151217_1606'), (b'converse', '0005_auto_20151221_1047'), (b'converse', '0006_deliverystatus'), (b'converse', '0007_order_price'), (b'converse', '0008_auto_20160314_1901'), (b'converse', '0009_appkey')]

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Delivery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('serial', models.CharField(max_length=50, null=True, verbose_name='\u5feb\u9012\u5355\u53f7', blank=True)),
                ('started', models.DateField(default=django.utils.timezone.now, verbose_name='\u5f00\u59cb\u65e5\u671f')),
                ('image', models.ImageField(upload_to=b'', null=True, verbose_name='\u5feb\u9012\u5355\u7167\u7247', blank=True)),
                ('comment', models.CharField(max_length=200, null=True, verbose_name='\u6ce8\u91ca', blank=True)),
            ],
            options=None,
            bases=None,
        ),
        migrations.CreateModel(
            name='DeliveryCompany',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
            options=None,
            bases=None,
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('done', models.BooleanField(default=False, verbose_name='\u8ba2\u5355\u5df2\u5b8c\u6210\uff1f')),
                ('detail', models.CharField(max_length=1000, null=True, verbose_name='\u8be6\u5355', blank=True)),
            ],
            options=None,
            bases=None,
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=6, verbose_name='\u540d\u5b57')),
                ('level', models.CharField(default=b'', max_length=15, null=True, verbose_name='\u6392\u5e8f', blank=True)),
                ('address', models.TextField(max_length=200, null=True, verbose_name='\u5730\u5740', blank=True)),
                ('proof', models.CharField(max_length=18, null=True, verbose_name='\u8eab\u4efd\u8bc1\u53f7\u7801', blank=True)),
                ('phone', models.CharField(max_length=40, null=True, verbose_name='\u7535\u8bdd\u53f7\u7801', blank=True)),
                ('image_front', models.ImageField(upload_to=b'', null=True, verbose_name='\u8eab\u4efd\u8bc1\u6b63\u9762', blank=True)),
                ('image_back', models.ImageField(upload_to=b'', null=True, verbose_name='\u8eab\u4efd\u8bc1\u53cd\u9762', blank=True)),
                ('description', models.TextField(default=b'', max_length=200, null=True, verbose_name='\u6ce8\u91ca', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Stuff',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name='\u82f1\u6587\u540d')),
                ('name_c', models.CharField(max_length=200, verbose_name='\u4e2d\u6587\u540d')),
                ('brand', models.CharField(max_length=40, verbose_name='\u54c1\u724c')),
                ('level', models.CharField(default=b'', max_length=15, null=True, verbose_name='\u6392\u5e8f', blank=True)),
                ('price', models.DecimalField(default=0, verbose_name='\u4ef7\u683c', max_digits=6, decimal_places=2)),
                ('dose', models.CharField(max_length=100, null=True, verbose_name='\u5242\u91cf', blank=True)),
                ('barcode', models.CharField(max_length=50, null=True, verbose_name='\u6761\u5f62\u7801', blank=True)),
                ('image', models.ImageField(upload_to=b'', null=True, verbose_name='\u56fe\u7247', blank=True)),
                ('comment', models.CharField(max_length=1000, null=True, verbose_name='\u6ce8\u91ca', blank=True)),
            ],
            options=None,
            bases=None,
        ),
        migrations.AddField(
            model_name='order',
            name='person',
            field=models.ForeignKey(verbose_name='\u987e\u5ba2', to='converse.Person'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='delivery',
            name='order',
            field=models.ForeignKey(verbose_name='\u8ba2\u5355', to='converse.Order'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='paid',
            field=models.BooleanField(default=False, verbose_name='\u5df2\u4ed8\u6b3e\uff1f'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='started',
            field=models.DateField(auto_now_add=True, verbose_name='\u65e5\u671f', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='detail',
            field=models.TextField(max_length=1000, null=True, verbose_name='\u8be6\u5355', blank=True),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='order',
            name='done',
        ),
        migrations.AddField(
            model_name='order',
            name='received',
            field=models.BooleanField(default=False, verbose_name='\u5df2\u6536\u8d27\uff1f'),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='DeliveryStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True, null=True)),
                ('d_date', models.CharField(max_length=50, verbose_name='\u66f4\u65b0\u65e5\u671f')),
                ('d_location', models.CharField(max_length=50, verbose_name='\u5730\u70b9')),
                ('d_detail', models.CharField(max_length=50, verbose_name='\u8be6\u60c5')),
                ('delivery', models.ForeignKey(verbose_name='\u5feb\u9012', to='converse.Delivery')),
            ],
            options=None,
            bases=None,
        ),
        migrations.AddField(
            model_name='order',
            name='price',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=6, blank=True, null=True, verbose_name='\u603b\u4ef7'),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='PersonGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=6, verbose_name='\u7fa4\u540d')),
            ],
            options=None,
            bases=None,
        ),
        migrations.AddField(
            model_name='person',
            name='group',
            field=models.ForeignKey(verbose_name='\u7fa4\u7ec4', blank=True, to='converse.PersonGroup', null=True),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='AppKey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('app', models.CharField(max_length=50, verbose_name='App')),
                ('key', models.CharField(max_length=50, verbose_name='Key')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options=None,
            bases=None,
        ),
    ]
