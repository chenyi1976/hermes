# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-27 09:21
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('converse', '0002_auto_20160522_0708'),
    ]

    operations = [
        migrations.CreateModel(
            name='Courier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('detail', models.TextField(blank=True, max_length=800, null=True)),
                ('serial_regex', models.TextField(blank=True, max_length=200, null=True)),
                ('rank', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('status_url', models.TextField(blank=True, max_length=400, null=True)),
                ('url_fields', models.TextField(blank=True, max_length=400, null=True)),
                ('is_post', models.BooleanField(default=True)),
                ('xpath_date', models.TextField(blank=True, max_length=400, null=True)),
                ('xpath_location', models.TextField(blank=True, max_length=400, null=True)),
                ('xpath_detail', models.TextField(blank=True, max_length=400, null=True)),
            ],
        ),
        migrations.DeleteModel(
            name='DeliveryCompany',
        ),
        migrations.AlterField(
            model_name='delivery',
            name='serial',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='\u5355\u53f7'),
        ),
        migrations.AddField(
            model_name='delivery',
            name='courier',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='converse.Courier', verbose_name='\u5feb\u9012'),
        ),
    ]
