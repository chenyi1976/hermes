$(function() {
    $('#confirm-div').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        $(this).find('.confirm-message').html($(e.relatedTarget).data('message'));
    });
});
