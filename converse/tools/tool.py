import sys
import os
import argparse
import time
import logging

sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))

from converse.tools.suck_web_to_db import suck_delivery_status_to_db
from converse.tools import setting

if __name__ == '__main__':
    if not os.path.exists("logging_dir"):
        os.mkdir("logging_dir")

    logging.basicConfig(filename='logging_dir\converse_' + time.strftime('%m%d%H%M') + '.log', level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument("mode", help="mode, supported value: prod / test")
    # parser.add_argument("-s", "--import_source_dir", help="import source dir ")
    # parser.add_argument("-r", "--recursive", help="recursive sub folders")
    # parser.add_argument("-z", "--zip_include", help="include zip files")
    args = parser.parse_args()

    mode = args.mode
    # import_source_dir = args.import_source_dir

    # validation
    # if import_source_dir is None:
    #     print 'import_source_dir must be provided.'
    #     sys.exit()
    #
    # if not os.path.isdir(import_source_dir):
    #     print 'import_source_dir must be a directory.'
    #     sys.exit()
    #
    # if not os.listdir(import_source_dir):
    #     print 'import_source_dir must not empty.'
    #     sys.exit()

    if mode == 'prod':
        logging.info('prod')
        setting.update_setting({'DEBUG': False})
        suck_delivery_status_to_db()
    elif mode == 'test':
        logging.info('test')
        setting.update_setting({'DEBUG': True})
        suck_delivery_status_to_db()
