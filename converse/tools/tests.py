from django.test import TestCase

from converse.models import Delivery, Order, Person
from converse.tools.suck_web import suck_web


class SuckWebTestCase(TestCase):
    def setUp(self):
        p = Person.objects.create(name='abc')
        o = Order.objects.create(person=p, detail='def')
        d = Delivery.objects.create(order=o, serial='9789357957800')

    def test_suck_web(self):
        d = Delivery.objects.get(serial='9789357957800')
        result = suck_web([(d.id, d.serial)])
        self.assertNotEqual(len(result), 0)
