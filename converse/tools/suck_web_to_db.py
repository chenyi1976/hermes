import sqlite3
import time

from converse.tools.setting import *
from converse.tools.suck_web import suck_web


def suck_delivery_status_to_db():
    db_file = get_file_db()
    print 'db_file:{}'.format(db_file)
    conn = sqlite3.connect(db_file)
    cursor = conn.cursor()

    today = time.strftime('%Y-%m-%d %H:%M:%S')

    cursor.execute("SELECT id, serial FROM converse_delivery "
                   "WHERE order_id IN (SELECT id FROM converse_order WHERE received = 0) ORDER BY id")

    deliveries = cursor.fetchall()

    data_list = suck_web(deliveries)
    print '{} deliveries, {} successful queried.'.format(len(deliveries), len(data_list))
    for delivery, data in data_list.iteritems():
        d_date = unicode(data[0])
        d_location = unicode(data[1])
        d_detail = unicode(data[2])

        cursor.execute('INSERT INTO converse_deliverystatus(date, delivery_id, d_date, d_location, d_detail)'
                       ' VALUES (?, ?, ?, ?, ?)',
                       (today, delivery[0], d_date, d_location, d_detail))
    conn.commit()
    conn.close()
