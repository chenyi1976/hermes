import os
import sys

# auto set debug mode, based on terminal (debug on) or contab (debug off)
import shutil

DEBUG = os.isatty(sys.stdin.fileno())


def get_file_db():
    if DEBUG:
        file_db = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../test_converse.sqlite3")
        if not os.path.exists(file_db):
            shutil.copyfile(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../converse.sqlite3"), file_db)
    else:
        file_db = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../converse.sqlite3")

    return file_db


def update_setting(settings):
    if type(settings) is dict:
        for key in settings:
            if key in globals():
                globals()[key] = settings[key]
