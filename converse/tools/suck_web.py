from lxml import etree
from nntplib import NNTPReplyError
import requests
import time


def suck_web(deliveries):
    retry_max_count = 3
    result = {}

    # session = requests.Session()
    # session.mount("http://", requests.adapters.HTTPAdapter(max_retries=3))
    # session.mount("https://", requests.adapters.HTTPAdapter(max_retries=1))

    for delivery in deliveries:

        # delivery_id = delivery[0]
        delivery_serial = delivery[1]
        if delivery_serial[:2] == "SS" or delivery_serial[:2] == "BS" \
                or delivery_serial[:2] == "SD" or delivery_serial[:3] == "TYS":
            page_url = "http://track.blueskyexpress.com.au/cgi-bin/GInfo.dll?EmmisTrack"
            data = {'w': "aaeexpress", "cno": delivery_serial}
            xpath_date = "//table[@id='oTHtable']//tr[last()]/td[1]"
            xpath_location = "//table[@id='oTHtable']//tr[last()]/td[2]"
            xpath_detail = "//table[@id='oTHtable']//tr[last()]/td[3]"
        elif delivery_serial[:3] == "SHL" or delivery_serial[:3] == "SCY":
            page_url = "https://www.shlexp.com/track.html"
            data = {'cno': delivery_serial}
            xpath_date = "//div[@id='oDetail']//tr[last()]/td[1]"
            xpath_location = "//div[@id='oDetail']//tr[last()]/td[2]"
            xpath_detail = "//div[@id='oDetail']//tr[last()]/td[3]"
        elif len(delivery_serial) == 13:
            page_url = "http://kd.hitaoe.com/main?wtl=shipTrack&code=" + delivery_serial
            data = {}
            xpath_date = "//tbody//tbody/tr[last()]/td[1]"
            xpath_location = "//tbody//tbody/tr[last()]/td[3]"
            xpath_detail = "//tbody//tbody/tr[last()]/td[2]"
        elif len(delivery_serial) == 12 or delivery_serial[:1] == "A":
            page_url = "http://www.zhexpress.com.au/TOrderQuery_Service.aspx"
            data = {"OrderId": delivery_serial}
            xpath_date = "//tbody[@class='ig_LaytonItem igg_LaytonItem']/tr[last()]/td[1]/span"
            xpath_location = "//tbody[@class='ig_LaytonItem igg_LaytonItem']/tr[last()]/td[2]/span"
            xpath_detail = "//tbody[@class='ig_LaytonItem igg_LaytonItem']/tr[last()]/td[3]/span"
        elif len(delivery_serial) == 10:
            page_url = "http://www.auodexpress.com/website/doAuodList.do"
            data = {"waybillNum": delivery_serial}
            xpath_date = "//table[@id='logs']//table/tr[1]/td[1]"
            xpath_location = "//table[@id='logs']//table/tr[1]/td[2]"
            xpath_detail = "//table[@id='logs']//table/tr[1]/td[2]"

        # response = requests.post(page_url, data={'w':"aaeexpress", "cno": "BS103138"})
        response = None
        retry_count = 0
        while response is None and retry_count < retry_max_count:

            # sleep 3 seconds.
            time.sleep(3)
            retry_count += 1

            try:
                if data:
                    response = requests.post(page_url, data)
                else:
                    response = requests.get(page_url)
            except:
                pass

        if response is None:
            print 'fail: url can not be opened for {}'.format(delivery_serial)
            continue

        the_page = response.text

        page = etree.HTML(the_page)

        try:
            d_date = page.xpath(xpath_date)[0].text
            if d_date:
                d_date.strip()
            d_location = page.xpath(xpath_location)[0].text
            if d_location:
                d_location.strip()
            d_detail = page.xpath(xpath_detail)[0].text
            if d_detail:
                d_detail.strip()
            result[delivery] = (d_date, d_location, d_detail)
            print (delivery_serial, d_date, d_location, d_detail)
        except NNTPReplyError:
            print 'NNTPReplyError: can not get data for given xpath for {}'.format(delivery_serial)
        except IndexError:
            print 'IndexError: can not get data for given xpath for {}'.format(delivery_serial)
        except AttributeError:
            print 'AttributeError: can not get data for given xpath for {}'.format(delivery_serial)
        except Exception:
            print 'fail: can not get data for given xpath for {}'.format(delivery_serial)

    return result
