from django.contrib import admin

from converse.models import *

admin.site.register(Stuff)
admin.site.register(Person)
admin.site.register(Delivery)
admin.site.register(DeliveryStatus)
admin.site.register(Order)
admin.site.register(Courier)
admin.site.register(PersonGroup)
admin.site.register(AppKey)
