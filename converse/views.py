from django.contrib.auth.forms import UserCreationForm
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.views.generic import CreateView, ListView, View, UpdateView
from django.views.generic.edit import ModelFormMixin, FormMixin
from django.views.generic.list import MultipleObjectMixin

from converse.models import *


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect("/")
    else:
        form = UserCreationForm()

    context = {'form': form}
    return render(request, 'registration/register.html', context)


def confirm_paid(request, order_id):
    if order_id:
        order = Order.objects.get(pk=order_id)
        if order:
            order.paid = True
            order.save()
    return HttpResponseRedirect('/order')


def confirm_unpaid(request, order_id):
    if order_id:
        order = Order.objects.get(pk=order_id)
        if order:
            order.paid = False
            order.save()
    return HttpResponseRedirect('/order')


def confirm_received(request, order_id):
    if order_id:
        order = Order.objects.get(pk=order_id)
        if order:
            order.received = True
            order.save()
    return HttpResponseRedirect('/order')


def confirm_unreceived(request, order_id):
    if order_id:
        order = Order.objects.get(pk=order_id)
        if order:
            order.received = False
            order.save()
    return HttpResponseRedirect('/order')


def confirm_paid_received(request, order_id):
    if order_id:
        order = Order.objects.get(pk=order_id)
        if order:
            order.received = True
            order.paid = True
            order.save()
    return HttpResponseRedirect('/order')


def delivery_status_list(request, delivery_id):
    if delivery_id:
        delivery = Delivery.objects.get(pk=delivery_id)
        status_list = DeliveryStatus.objects.filter(delivery=delivery)
        context = {'status_list': status_list}
        return render(request, 'converse/delivery_status_list.html', context)

    return HttpResponseRedirect('/order')


def order_report(request):
    show_group = request.GET.get('show_group', '')
    order_list = Order.objects.raw(
        'SELECT * FROM converse_order where received = 0 and id not in (select order_id from converse_delivery)')
    context = {'order_list': order_list, 'show_group': show_group}
    return render(request, 'converse/order_report.html', context)


def api_order_list(request):
    key = request.GET.get('key', '')
    app_key = AppKey.objects.filter(key=key)
    if app_key:
        orders = Order.objects.filter(Q(paid=False) | Q(received=False))
        json_dict = {}
        for order in orders:
            order_dict = {'person': order.person.name, 'detail': order.detail}
            json_dict[order.id] = order_dict
        return JsonResponse(json_dict)
    return JsonResponse({})


def api_order_add(request):
    key = request.GET.get('key', '')
    app_key = AppKey.objects.get(key=key)
    if app_key:
        person_id = request.GET.get('person_id', '')
        person = Person.objects.get(id=person_id)
        if person:
            detail = request.GET.get('detail', '')
            order = Order()
            order.person = person
            order.detail = detail
            order.save()

            return JsonResponse({order.id: {'person': person.name, 'detail': detail}})
    return JsonResponse({})


def api_order_delivery(request):
    key = request.GET.get('key', '')
    app_key = AppKey.objects.get(key=key)
    if app_key:
        order_id = request.GET.get('order_id', '')
        order = Order.objects.get(id=order_id)

        deliveries = Delivery.objects.filter(order=order)
        json_dict = {}
        for delivery in deliveries:
            json_dict[delivery.id] = delivery.serial
        return JsonResponse(json_dict)
    return JsonResponse({})


def api_order_delivery_add(request):
    key = request.GET.get('key', '')
    app_key = AppKey.objects.get(key=key)
    if app_key:
        order_id = request.GET.get('order_id', '')
        order = Order.objects.get(id=order_id)
        serial = request.GET.get('serial', '')
        if order and serial:
            delivery = Delivery()
            delivery.order = order
            delivery.serial = serial
            delivery.save()

            return JsonResponse({delivery.id: {'order': order.id, 'serial': serial}})
    return JsonResponse({})


def api_order_delivery_remove(request):
    key = request.GET.get('key', '')
    app_key = AppKey.objects.get(key=key)
    if app_key:
        delivery_id = request.GET.get('delivery_id', '')
        delivery = Delivery.objects.get(id=delivery_id)
        serial = delivery.serial
        if delivery:
            delivery.delete()
            return JsonResponse({delivery_id: serial})
    return JsonResponse({})


class DefaultUserMixin(ModelFormMixin, View):
    def form_valid(self, form):
        user = self.request.user
        form.instance.user = user
        return super(DefaultUserMixin, self).form_valid(form)


class PersonCreateView(DefaultUserMixin, CreateView):
    model = Person
    fields = ['name', 'level', 'phone', 'proof', 'address', 'group']
    template_name = "converse/form_detail.html"
    success_url = '/order'


class PersonFieldFilterMixin(FormMixin, View):
    def get_form(self, form_class=None):
        form = super(PersonFieldFilterMixin, self).get_form(form_class)
        form.fields['person'].queryset = Person.objects.filter(user=self.request.user)
        return form


class OrderCreateView(DefaultUserMixin, PersonFieldFilterMixin, CreateView):
    model = Order
    fields = ['person', 'detail', 'price', 'paid', 'received']
    template_name = "converse/form_detail.html"
    success_url = '/order'


class OrderUserFieldFilterUpdateView(PersonFieldFilterMixin, UpdateView):
    model = Order
    fields = ['person', 'detail', 'price', 'paid', 'received']
    template_name = "converse/form_detail.html"
    success_url = '/order'


class OrderFieldFilterMixin(FormMixin, View):
    def get_form(self, form_class=None):
        form = super(OrderFieldFilterMixin, self).get_form(form_class)
        form.fields['order'].queryset = Order.objects.filter(user=self.request.user)
        return form


class DeliveryCreateView(DefaultUserMixin, OrderFieldFilterMixin, CreateView):
    model = Delivery
    fields = ['order', 'serial']
    template_name = "converse/form_detail.html"
    success_url = '/order'

    def get_initial(self):
        order_id = self.request.GET.get('order_id', '')
        if order_id:
            order = Order.objects.get(id=order_id)
            return {
                'order': order,
            }


class DeliveryUpdateView(OrderFieldFilterMixin, UpdateView):
    model = Delivery
    fields = ['order', 'serial', 'courier']
    template_name = "converse/form_detail.html"
    success_url = '/order'

    def get_initial(self):
        order_id = self.request.GET.get('order_id', '')
        if order_id:
            order = Order.objects.get(id=order_id)
            return {
                'order': order,
            }


class FilterByUserMixin(MultipleObjectMixin, View):
    def get_queryset(self):
        if self.queryset is not None:
            return self.queryset.filter(user=self.request.user)
        else:
            return self.model.objects.all()


class ListViewByUser(FilterByUserMixin, ListView):
    pass
