from django.test import TestCase
from converse.models import Person, Order, Delivery
from converse.tools.suck_web_to_db import suck_delivery_status_to_db


class ConverseTestCase(TestCase):
    def setUp(self):
        Person.objects.create(name='sean')

    def test_person(self):
        p = Person.objects.get(name='sean')
        self.assertEqual(p.name, 'sean')


class SuckTestCase(TestCase):
    def setUp(self):
        person = Person.objects.create(name='sean')
        order = Order.objects.create(person=person)
        Delivery.objects.create(order=order, serial="BS296755")

    def test_import(self):
        suck_delivery_status_to_db()
        # png_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "test", "ok.png")
        # shutil.copyfile(png_path, os.path.join(import_directory, "ok.png"))
        # folder_name = os.path.split(import_directory)[1]
        #
        # run_import(connection, import_directory, None)
        #
        # pics = Picture.objects.all()
        # self.assertEqual(len(pics), 1)
        # self.assertEqual(pics[0].w, 32)
        # self.assertEqual(pics[0].h, 32)
        #
        # tags = pics[0].tags
        #
        # self.assertTrue(folder_name in tags.names())

