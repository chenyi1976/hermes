# README #

Hermes is a tool to manage your orders, then track your delivery.

### Features ###

* Customer address management
* Purchase order/payment tracking
* Delivery tracking, update progress twice a day for all your delivery
* Multi accounts support

### Screenshot ###

![screenshot1](https://bytebucket.org/chenyi1976/hermes/raw/22c5416a8e22550dd928e9d7784db5eb667c1e86/screenshot/hermes_screenshot1.png "screenshot1")