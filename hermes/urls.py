from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^accounts/', include(admin.site.urls)),
    url(r'^', include('converse.urls', namespace='c')),
]
